## Requirement 
1.	User melakukan registrasi dengan data diri,email, nomor telepon dan upload foto beserta KTP
2.	User dapat login dengan password atau biometric (jika ada di perangkat mobilenya)
3.	User dapat melihat Sisa hutang dan tagihan perbulan yang harus di bayarkan (Jika ada)
4.	User dapat meminjam uang paling besar Rp. 12.000.000 dengan tenor maksimal 1 tahun.
5.	User dalam proses peminjaman akan di proses dengan hasil diterima atau ditolak
6.	Jika pinjaman diterima maka akan ada notifikasi lewat email dan nomor telepon yang terdaftar
7.	User tidak dapat melakukan peminjaman uang jika sedang ada proses peminjaman dan belum di lunaskan.

## High Level Architecture 
![High Level Architecture](./picture/HDA.png)

## Screen Flow & Entity Relationship Diagram
Screen Flow<br> 
![Screen Flow](./picture/SF.png)

Entity Relationship Diagram<br>
![Entity Relationship Diagram](./picture/ERD.png)

## API Design 
![API Design](./picture/API.png)

## Screan Behavior
Registration <br>
![Registration](./picture/Registration.jpg)
<br>Profile & Biometric Activation<br>
![Profile & Biometric Activation](./picture/Biometric_Activation.jpg)
<br>History<br>
![History](./picture/History.jpg)
<br>Transaction (Loan & Payment)<br>
![HistTransaction (Loan & Payment)](./picture/Transaction.jpg) 
 
